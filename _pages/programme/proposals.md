---
layout: page
title: Proposals
permalink: /programme/proposals/
sponsors: true
---

## Important Information

 * Call for Proposals Opens: 9 August 2021
 * Call for Proposals Closes: ~~5 September 2021~~ **Extended to 11:59pm 12 September 2021** [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
 * Conference Opens: 14 January 2022

## How to submit

Our call for sessions and miniconfs is now closed.
Thank you to everyone who submitted a talk, tutorial or miniconf proposal.
Notifications from the sessions committee will be sent out in October 2021.

If you would like to review your proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## About linux.conf.au

linux.conf.au is a conference where people gather to learn about the entire world of Free and Open Source Software, directly from the people who contribute.
Many of these contributors give scheduled presentations, but much interaction occurs in-between and after formal sessions between all attendees.
Our aim is to create a deeply technical conference where we bring together industry leaders and experts on a wide range of subjects.

linux.conf.au welcomes submissions from first-time and seasoned speakers, from all free and open technology communities, and all walks of life.
We respect and encourage diversity at our conference.

## Conference Theme

{% include theme.md %}

Please let this inspire you, but not restrict you - we will still have many talks about other interesting topics in open source technologies.

Deciding what to speak about at linux.conf.au can be a tough challenge, particularly for new speakers.
If you would like some ideas on how to write a talk and how to submit a proposal, we recommend watching E. Dunham's [You Should Speak](https://www.youtube.com/watch?v=3QIQNcGnXes) talk from linux.conf.au 2018.

## Conference Location

linux.conf.au 2022 will be a fully online, virtual experience.
This means our speakers will be beaming in from their own homes or workplaces.
The organising team will be able to help speakers with their tech set-up.
Each accepted presenter will have a tech check prior to the event to smooth out any difficulties and there will be an option to pre-record the presentations.

## Proposal Types

We are accepting submissions for different types of proposal:

 * Talk (45 minutes): These are generally presented in lecture format and form the bulk of the available conference slots.
 * Miniconf Talk (30 minutes): Miniconfs are single-track mini-conferences that run on the first day of linux.conf.au. Miniconf Talks are similar in format to a main conference talk, however the topic will need to fit within the scope of one of the [Miniconfs](/programme/miniconfs/).
 * ~~Miniconf (full day): Single-track mini-conferences that run for the duration of the first day of linux.conf.au. We provide the room, and you provide the speakers. Together, you can explore a field in Open Source technologies in depth.~~ Submissions are now closed.

## <a name="proposer-recognition"></a> Proposer Recognition

In recognition of the value that presenters and Miniconf organisers bring to our conference, each accepted proposal will be entitled to:

 * *One* complimentary ticket to attend the conference
 * Optionally, recognition as a Contributor, available at a discounted rate

If your proposal includes more than one presenter or organiser, the additional people will be entitled to:

 * Professional or Contributor registration at a discounted rate

All participants in a presentation must have a valid ticket to the conference or they will not be able to attend.

As a volunteer-run non-profit conference, linux.conf.au does not pay speakers to present at the conference.

## Accessibility

linux.conf.au aims to be accommodating to everyone who wants to attend or present at the conference.
We recognise that some people face accessibility challenges.
If you have special accessibility requirements, you can provide that information when submitting your proposal so that we can plan to properly accommodate you.

## Code of Conduct

By agreeing to present at or attend the conference you are agreeing to abide by the [terms and conditions](/attend/terms-and-conditions/).
We require all speakers and delegates to have read, understood, and act according to the standards set forth in our [Code of Conduct](/attend/code-of-conduct/).

## Recording

To increase the number of people that can view your presentation, linux.conf.au will record your talk and make it publicly available after the event.
We plan to release recordings of every talk at the conference under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)) licence.
When submitting your proposal you may note that you do not wish to have your talk released, although we prefer and encourage all presentations to be recorded.

## Licensing

If the subject of your presentation is software, you must ensure the software has an Open Source Initiative-approved licence at the time of the close of our Call for Sessions.

## FAQ

**What is the difference between private and public summary?**

Private Summary is the portion that will be shown to the review team.
Give some more details of the talk and why it should be chosen.

Public Summary is the portion that will be shown to everyone on the website once the schedule is announced.
Provide enough details of the talk to whet the appetite.
