---
layout: page
title: Events
permalink: /programme/events/
sponsors: true
---

## LCA Local

We know many of you have missed the experience of a face-to-face conference and in 2022 we are launching LCA Local.
While our conference will be online, we are inviting people to join others in their local area and participate in the conference together.

Delegates attending a LCA Local event will **need a conference ticket**, so please make sure you have [purchased one](/attend/tickets/) prior to attending.
All events come under the linux.conf.au [Code of Conduct](/attend/code-of-conduct/).
Local restrictions and requirements will be followed as well, eg. mask wearing, check-ins, etc.

### Want to run a LCA Local event?

If you are interested in running a LCA Local event please [contact us](mailto:contact@lca2022.linux.org.au) to let us know details so we can help to promote it.
You are also welcome to [submit a change](https://gitlab.com/laconfdev/lca2022-website/-/blob/main/_pages/programme/events.md) to this page with the details.
## Canberra

**UPDATE: Unfortunately the LCA Local event at Canberra has been cancelled due to the current COVID-19 situation.**

Delegates in Canberra are invited to attend a LCA Local event at [Kambri](https://kambri.com.au/about/), which will run for the duration of linux.conf.au 2022.
Presentations will be shown in the room to allow people to attend streams together.

You may come for one day, all three days or any part you would like.
There will be catering provided for morning and afternoon tea.
Dietary requirements will be covered provided you register to attend soon enough.

To register for the Canberra LCA Local event, please go to your [Dashboard](/dashboard/) and select the [LCA Local](/tickets/category/5) category under 'Add/Update Items'.
Once there, select which days you would like to attend the event.
This is required to ensure catering can meet everyone's dietary requirements.


