---
layout: page
title: Open Hardware Miniconf
permalink: /programme/miniconfs/open-hardware/
sponsors: true
---

<p class="lead">
Organised by Jonathan Oxer and Andy Gelme
</p>

## When

Friday 14 January 2022

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="open-hardware-miniconf" day="friday" closed="True" %}

## About

One of the drivers of the popularity of the Open Hardware community is easy access to inexpensive and capable microcontrollers and development boards such as the Arduino and ESP32.

In 2022, for the first time at OHMC, we're diving into the wonderful world of FPGAs... and fully embracing the LCA2022 theme of community.

Not only do FPGAs open up new capabilities for your projects, but they also help bring Open Source further down the stack closer to the silicon.
It is easier than ever to run Open Source software on an Open Source processor core (RISC-V) that you can hack yourself, and install directly onto an FPGA using an Open Source FPGA development tool chain.

If you've never used an FPGA before, or don't even know what it stands for, don't worry.
We'll take you through the basics and get you started with a Simple Add-On (SAO) that can be used standalone or plugged into many different electronic conference badges.
If you have an LCA2021 SwagBadge you can combine it with the LCA2022 SAO to build a musical project that will demonstrate the unique capabilities of FPGAs.

We'll also have an updated LCA2022 SwagBadge available in case you didn't get one last year.
There will be opportunities in the months leading up to LCA2022 for you to participate in online workshops and local community events to learn about skills such as soldering, PCB design (your own SAO) and developing applications for your SwagBadge / SAOs (both 2021 and 2022 variants).

The mini-conference day will run in two distinct halves.
The morning sessions will discuss the design of the LCA2022 FPGA SAO and SwagBadge applications, explaining how to use the hardware and firmware that runs on them.
The afternoon sessions will be presentations about more general Open Hardware topics, with contributions by both LCA2022 attendees and the OHMC team.

More information about the online workshops and community activities leading up to LCA2022 will be posted at [www.openhardwareconf.org](http://www.openhardwareconf.org) throughout the year.
