---
layout: page
title: Kernel Miniconf
permalink: /programme/miniconfs/kernel/
sponsors: true
---

<p class="lead">
Organised by Andrew Donnellan
</p>

## When

Friday 14 January 2022

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="kernel-miniconf" day="friday" closed="True" %}

## About

The Kernel Miniconf is a single-day Miniconf track about everything related to the kernel and low-level systems programming.

The Kernel Miniconf invites talks about up-and-coming kernel developments, the future direction of the kernel, and kernel development community and process matters.
Past Kernel Miniconfs have covered topics such as memory management, RCU, scheduling, testing/CI and filesystems, as well as community and process topics such as licensing, developer workflows, safety critical processes, and so on.

We invite submissions on anything related to kernel and low-level systems programming.
We welcome submissions from developers of all levels of experience, and from anyone connected with the kernel whether you are an upstream kernel developer, distro maintainer, academic researcher or a developer who works further downstream.
The focus of the Miniconf will primarily be on Linux, however non-Linux talks of sufficient interest to a primarily Linux audience will be considered.

Information about previous Kernel Miniconfs can be found at [lca-kernel.ozlabs.org](https://lca-kernel.ozlabs.org).
