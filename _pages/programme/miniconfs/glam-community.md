---
layout: page
title: GO GLAM meets Community Miniconf
permalink: /programme/miniconfs/glam-community/
sponsors: true
---

<p class="lead">
Organised by Sae Ra Germaine
</p>

## When

Friday 14 January 2022

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="glam-community-miniconf" day="friday" closed="True" %}

## About

This Miniconf will explore the intersection of galleries, archives, libraries and museums (GLAM) and community.
We will explore a range of topics related to GLAM, while also diving into discussions about open source communities, similar to that of past Community Leadership Summits (CLSx).
