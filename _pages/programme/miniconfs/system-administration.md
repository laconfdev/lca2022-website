---
layout: page
title: System Administration Miniconf
permalink: /programme/miniconfs/system-administration/
sponsors: true
---

<p class="lead">
Organised by Simon Lyall and Ewen McNeill
</p>

## When

Friday 14 January 2022

## Call for Sessions

{% include miniconf_cfs.md miniconf_slug="sysadmin-miniconf" day="friday" closed="True" %}

## About

The [Systems Administration Miniconf](https://sysadmin.miniconf.org/) focuses on professional management of real-world Linux and open source environments, both large and small.
The Miniconf aims to include a diverse range of tools and techniques that will help keep your entire environment functioning smoothly, and accomplish more with less effort.
An important goal will be to provide talks directly useful to professional Linux administrators.

This is the sixteenth year that the Sysadmin Miniconf has featured at linux.conf.au, after a year that has brought some of the most rapid changes to systems supporting user's work (including a very rapid surge in "work from home").

More information, including presentation slides and recordings from previous years, is available on the [Sysadmin Miniconf website](https://sysadmin.miniconf.org/).
