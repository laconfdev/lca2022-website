---
layout: page
title: Sessions
permalink: /programme/sessions/
sponsors: true
---

The main conference for linux.conf.au runs over two days - Saturday 15 and Sunday 16 January 2022.
This will be made up of talks on a wide range of topics related to open technologies.

To view the sessions selected for the main section of the conference, check out the [schedule](/schedule/).

### Talks
Talks are 35-45 minute presentations on a single topic.
These are generally presented in lecture format and form the bulk of the available conference slots.
