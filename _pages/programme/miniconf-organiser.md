---
layout: page
title: Information for Miniconf Organisers
permalink: /programme/miniconf-organiser/
sponsors: true
---

## Overview

Miniconf organisers are responsible for planning an running a day-long specialist track at linux.conf.au.
The linux.conf.au organisers will assist with many parts of the process to make the job simpler.

## Call for Miniconfs

The Call for Miniconfs is now closed.
Thank you to everyone who submitted a proposal.

 * Call for Miniconfs Opens: 9 July 2021
 * Call for Miniconfs Closes: 11:59pm 31 July 2021 [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
 * Conference Opens: 14 January 2022

If you would like to review your Miniconf proposals, you can continue to do so via your [Dashboard](/dashboard/).

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

## What is expected from a Miniconf organiser?

Miniconf organisers will need to arrange speakers to fill the schedule for their allocated day and room.
The conference team will provide a call for proposals system on the conference website for people to submit proposals for each Miniconf.
The proposal submission form can be adjusted per Miniconf as required, for example to ask additional questions around presentation format.
Miniconf organisers can then review the proposals for their Miniconf and accept sessions they would like to appear.
Once all session acceptances have been made, the Miniconf organiser will need to arrange the talks into a schedule for the day and provide this to the conference team for it to be published on the website.

## What is provided to speakers at the Miniconf?

Similar to the main conference, each accepted proposal will be given one ticket to attend linux.conf.au.
If a presentation has multiple speakers, these additional people will need to purchase a ticket, but at a discounted price.
Details of what is provided, including information on ticket discounts, is available on the [Proposals page](/programme/proposals/#proposer-recognition)

## What is the structure will the Miniconf day?

Miniconfs will start the day with a LCA overall housekeeping session then a keynote presentation.
After morning tea the Miniconfs will start their proceedings, then have eight 30 minute presentations across the rest of the day.
There will be a short break between each presentation to ensure we have adequate time to do a AV changeover.
Please note that times and durations for main breaks are slightly different to the main conference days.

The schedule will follow the template below:

{% include miniconf_schedule.html %}
