---
layout: page
title: Shirts and Swag
permalink: /attend/shirts/
sponsors: true
---

## Shirts (and other Swag)

Due to linux.conf.au 2022 being online, we are not offering shirts with our standard tickets.

But do not fear!
We know that many of you would still like to get a memento of attending LCA2022, so we have setup an online store for you to purchase shirts and other items (coffee mugs, shower curtains, etc).
To make things more exciting, we have two different designs available this year - the Sanitiser Tux and the LCA2022 Community.

<a href="https://www.redbubble.com/shop/ap/94005855" class="btn btn-outline-primary" role="button">Tux Shirts and Swag</a>
<a href="https://www.redbubble.com/shop/ap/94005406" class="btn btn-outline-secondary" role="button">Community Shirts and Swag</a>
