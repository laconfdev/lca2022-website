---
layout: page
title: Volunteer
permalink: /attend/volunteer/
card: call-volunteers.52e4ad24.png
sponsors: true
---

linux.conf.au is a grass-roots, community-driven event, which wouldn't be successful without the wonderful people who are willing to lend us a hand.
This year, we're again seeking volunteers to fulfil a variety of roles to make this conference a success.

With the conference being online, you can be located anywhere in the world, as long as you are willing to be online during the conference hours.
It is important to note that the conference timezone will be Australian Eastern Daylight Time (AEDT - UTC+11).
Depending on your role, most volunteers will be required from 8am on the days of the conference.

Volunteering at linux.conf.au is a great opportunity and can be deeply rewarding.
On a more practical note, volunteering at linux.conf.au can add to your professional portfolio in many ways, making this an ideal opportunity for students or recent graduates.

By volunteering at linux.conf.au, you will have the benefit of:

* Meeting exceptional people - many of whom are recognised experts in their field and industry luminaries
* Great networking opportunities with people working in the free and open source community
* Gaining practical, hands-on experience and skills in event management, customer liaison, public relations and operation of audio visual equipment - which are invaluable to future employers

## What's required of me?
To be eligible for Volunteer Registration at linux.conf.au you need to:

* be willing to commit to three full days of volunteering
* able to attend the mandatory training and induction prior to the conference

The following is a indication of the tasks available across the conference:

<div class="table-responsive">
<table class="table table-striped table-bordered" summary="This table provides a summary of the days volunteers will be needed">
  <tr>
    <th>Day</th>
    <th>Tasks available</th>
  </tr>
  <tr>
    <td>Friday 14 January</td>
    <td>Miniconf room monitoring, AV and general assistance</td>
  </tr>
  <tr>
    <td>Saturday 15 January</td>
    <td>Conference room monitoring, MC, AV and general assistance</td>
  </tr>
  <tr>
    <td>Sunday 16 January</td>
    <td>Conference room monitoring, MC, AV and general assistance</td>
  </tr>
</table>
</div>

**We would also like you to have one or more of the following skills and/or aptitudes:**

* the ability to meet and greet people and provide assistance with a friendly, cheerful disposition
* some familiarity with audio visual technologies
* confidence addressing an audience, such as introducing a speaker
* some familiarity with technology such as IRC, social media, text editors, etc

## Role Details

* Speaker Wrangler
  * Monitor for speaker check-in in the designated place
  * Coordinate speakers on the backstage -⁠ make sure they are there and direct them to where they should be
  * Escalate to the speaker liaison if speakers do not show up
  * Ask the speaker if they're bring any additional co-speakers
  * Update scheduling document as speakers check-in
* MC
  * Prior to the conference, fill out 'MC notes' in the run sheets to decide how to introduce each speaker
  * Chat to the speaker(s) 10-15 minutes before the talk starts to confirm how they wish to be introduced, whether they want to take questions, etc.
  * Introduce speakers to the audience at the beginning of each presentation
  * Time keeping for the talk: give 5, 2, 1-minute warnings
  * Thank speakers at the end
* AV
  * Look after video switching for each stream, perform tech checks to ensure speakers can present with decent quality audio
  * Volunteers with previous skills and experience in AV related work preferred
* Room Monitors
  * Responsible for monitoring the rooms within the conference venue, including watching the chat stream for issues and queries
  * Field 'rego desk' type of questions
  * Escalate to core team where necessary
  * For chat monitors assigned to a particular room, designate one person to monitor the chat for questions to the presenter

## What's in it for me?

Tempted, but want to know more about what you get for volunteering your time?
This is what we're offering:

* Free conference registration at Hobbyist level
* A food allowance to provide you with breakfast, lunch, or dinner, whichever you choose
* Written reference covering your experience and duties - useful for job interviews!

## Sign me up!

<a href="https://forms.gle/YE2Y8hNQpe8Hs8Cn6" class="btn btn-primary">Apply Now</a>

Any questions? Email us at [volunteers@lca2022.linux.org.au](mailto:volunteers@lca2022.linux.org.au)
