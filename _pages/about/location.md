---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The Venue

linux.conf.au 2022 will not have a fixed venue in 2022, as it will be delivered in an online format to everyone worldwide.

Stay tuned for more information on how to access the conference coming in the next few months.
