---
layout: page
title: Contact
permalink: /about/contact/
sponsors: true
---

## Contact the Organising Team

Is there something you would like to know that isn't on this site? Please contact us via email.

<i class="bi-envelope-fill"></i> Email: [contact@lca2022.linux.org.au](mailto:contact@lca2022.linux.org.au)

## Stay up to date

We have several channels that we use to post announcements in the lead up to, and during, the conference.

* <i class="bi-twitter"></i> Twitter: [@linuxconfau](https://twitter.com/linuxconfau), hashtag #lca2022
* <i class="bi-facebook"></i> Facebook: [@linuxconferenceaustralia](https://facebook.com/linuxconferenceaustralia)
* <i class="bi-linkedin"></i> LinkedIn: [linuxconfau](https://www.linkedin.com/company/linuxconfau/)
* <i class="bi-youtube"></i> YouTube: [linuxconfau](https://www.youtube.com/c/linuxconfau)
* <i class="bi-inboxes-fill"></i> Announce mailing list: [LCA Announce](https://lists.linux.org.au/mailman/listinfo/lca-announce)

## Contact Linux Australia
  Linux Australia is the auspice for linux.conf.au.
  Check out their website for more details and contact information [linux.org.au](https://linux.org.au)

## Chat with attendees

There are several ways you can chat with other people interested in linux.conf.au.

* Matrix: [#linux.conf.au](https://matrix.to/#/#linux.conf.au:matrix.org)
* IRC: #linux.conf.au on [libera.chat](https://libera.chat/)
* Chat mailing list: Please subscribe via your [Dashboard](/dashboard/) and wait for changes to be synchronised
