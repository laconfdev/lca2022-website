{% if include.closed %}
  Submissions to this Miniconf are now closed.

  <a class="btn btn-md btn-outline-primary" href="/schedule/#{{ include.day }}">View the schedule</a>
{% else %}
  {{ include.lead | default: "The call for sessions is now open!" }}

  * Call for Sessions Opens: 9 August 2021
  * Call for Sessions Closes: ~~5 September 2021~~ **Extended to 11:59pm 12 September 2021** [Anywhere on Earth (AoE)](https://en.wikipedia.org/wiki/Anywhere_on_Earth)
  * Conference Opens: 14 January 2022

  {% if include.miniconf_slug %}
  <a class="btn btn-md btn-outline-primary" href="/proposals/submit/{{ include.miniconf_slug }}/">Submit a proposal</a>
  {% endif %}
{% endif %}
