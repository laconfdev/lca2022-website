Our theme is community.

After the challenges of the past year, our community has explored ways to rebuild the connections we were used to having face-to-face.
Many Open Source communities already had their roots online, so how can this be applied to other areas, and how can we keep people interested as they shift to living even more of their lives online?
How can we keep in contact with connections in other countries in a virtual way?
