---
layout: post
title: "Call for Sessions now open!"
card: call-sessions.ae854df7.png
---

<p class="lead">The linux.conf.au 2022 Call for Sessions is now open. It will stay open until 11:59pm 5 September 2021 <a href="https://en.wikipedia.org/wiki/Anywhere_on_Earth">Anywhere on Earth (AoE)</a>.</p>

The theme for 2022 is 'community'.

After the challenges of the past year, our community has explored ways to rebuild the connections we were used to having face-to-face.
Many Open Source communities already had their roots online, so how can this be applied to other areas, and how can we keep people interested as they shift to living even more of their lives online?
How can we keep in contact with connections in other countries in a virtual way?

If you have ideas or developments you'd like to share with the open source community at linux.conf.au, we'd love to hear from you.

## Call for Sessions

The main conference runs on Saturday 15 and Sunday 16 January, with multiple streams catering for a wide range of interest areas.
We invite you to submit a [session proposal](../../programme/proposals/) on a topic you are familiar with.
Talks are 45 minute presentations on a single topic presented in lecture format.
Each accepted talk will receive one ticket to attend the conference.

## Call for Miniconfs

We are pleased to announce we will again have four Miniconfs on the first day, Friday 14 January 2022.
These are:

* GO GLAM meets Community
* Kernel
* Open Hardware
* System Administration

Based on feedback over a few years, we will be introducing two major changes for Miniconfs in 2022: all presentations will be 30 minutes long, and each accepted presentation will receive one ticket to the conference.
The Call for Miniconf Sessions is now open on our website, so we encourage you to submit your proposal today.
Check out our [Miniconfs page](../../programme/miniconfs/) for more information.

## No need to book flights or hotels

Don't forget: the conference will be an online, virtual experience.
This means our speakers will be beaming in from their own homes or workplaces.
The organising team will be able to help speakers with their tech set-up.
Each accepted presenter will have a tech check prior to the event to smooth out any difficulties and there will be an option to pre-record presentations.

## Introducing LCA Local

We know many of you have missed the experience of a face-to-face conference and in 2022 we are launching LCA Local.
While our conference will be online, we are inviting people to join others in their local area and participate in the conference together.
More information and an expression of interest form for LCA Local will be released soon.

## Have you got an idea?

You can find out how to submit your session proposals [here](../../programme/proposals/). If you have any other questions, you can contact us via [email](mailto:contact@lca2022.linux.org.au).

The session selection committee is looking forward to reading your submissions.
We would also like to thank them for coming together and volunteering their time to help put this conference together.

linux.conf.au 2022 Organising Team
