---
layout: post
title: "Keynote: Brian Kernighan"
card: keynote-brian.850eed1f.png
---

<p class="lead">Our fourth keynote for linux.conf.au 2022 is Brian Kernighan.</p>

<p><img class="img-fluid float-sm-right profile-pic" src="../../media/img/keynote/brian_kernighan.jpg" alt="Brian Kernighan" width="250" /></p>

Brian will be speaking about the early days of Unix at Bell Labs, how it came about, some of the good and bad ideas that eventuated from it, and where Unix is headed.

In barely 50 years, the Unix operating system has gone from a tiny two-person experiment in a New Jersey attic to a multi-billion dollar industry whose products and services are an integral part of the world's computing infrastructure.
Along the way, there have been many changes, but a surprisingly large amount is much the same as when it started.

How did this come about?
What are the good ideas in Unix that have been preserved and even spread?
What are the good ideas that have fallen by the wayside?
What are the not so good ideas that have prospered?
And what might the future hold?

## About Brian Kernighan

Brian Kernighan received his PhD from Princeton in 1969, and was in the Computing Science Research center at Bell Labs until 2000.
He is now a professor in the Computer Science Department at Princeton, where he writes short programs and longer books.
The latter are better than the former, and certainly need less maintenance.

We look forward to welcoming Brian at linux.conf.au 2022.
