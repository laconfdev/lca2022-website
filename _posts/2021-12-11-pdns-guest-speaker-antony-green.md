---
layout: post
title: "Special Guest Speaker: Antony Green"
card: keynote-antony.cf3ff3ce.png
---

<p class="lead">Delegates attending our Professional Delegates Networking Session will hear from our special guest speaker Antony Green.</p>

<p><img class="img-fluid float-sm-right profile-pic" src="../../media/img/keynote/antony_green.jpg" alt="Antony Green" width="250" /></p>

Antony will be joining us to talk about the statistical processes used to call Australian elections.
What is the available data, what assumptions are made to predict results, and how is the Australian process more robust than in the United States?

Our Professional level delegates will be able to hear Antony speak at the Professional Delegates Networking Session (PDNS) on the evening of Saturday 15 January.
If you have not yet bought a ticket to linux.conf.au 2022, now is a great time to [register to attend](../../attend/tickets/).
For those of you who are having a difficult time convincing your employer to let you attend, we have a helpful [guide](../../attend/why-should-employees-attend/) to assist.

## About Antony Green

Antony Green is best known as Chief Election Analyst with the Australian Broadcasting Corporation (ABC) and is the face of television election coverages in Australia.

Antony has worked for the ABC since 1989.
In that time he has worked on more than 80 federal, state and territory elections.
He has also worked on local government elections, numerous by-elections and covered elections in the United Kingdom, New Zealand and Canada.
Antony designed the ABC's election night computer analysis system.

Antony studied at the University of Sydney and was awarded a Bachelor of Science in Pure Mathematics and Computer Science, and a Bachelor of Economics with Honours in politics.
He was granted an Honorary Doctor of Letters by the University of Sydney in 2014 and appointed an Adjunct Professor in the University of Sydney's Department of Government and International Relations in 2015.

Antony was recognised in the 2017 Queen's birthday honours list when he was appointed an Officer in the Order of Australia.
The citation was "for distinguished service to the broadcast media as an analyst and commentator for state and federal elections, and to the community as a key interpreter of Australian democracy".

We look forward to welcoming Antony at linux.conf.au 2022.
