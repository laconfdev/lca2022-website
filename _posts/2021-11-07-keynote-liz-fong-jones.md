---
layout: post
title: "Keynote: Liz Fong-Jones"
card: keynote-liz.8c0e4310.png
---

<p class="lead">We are excited to announce that Liz Fong-Jones will be one of our keynote speakers for linux.conf.au 2022.</p>

<p><img class="img-fluid float-sm-right profile-pic" src="../../media/img/keynote/liz_headshot.jpg" alt="Liz Fong-Jones" width="250" /></p>

Liz will be speaking about the ways in which we can cultivate production excellence.
Taming the complex distributed systems we're responsible for requires changing not just the tools and technical approaches we use; it also requires changing who is involved in production, how they collaborate, and how we measure success.

In her talk, you'll learn about several practices core to production excellence: giving everyone a stake in production, collaborating to ensure observability, measuring with Service Level Objectives, and prioritizing improvements using risk analysis.


## About Liz Fong-Jones

Liz is a developer advocate, labour and ethics organizer, and Site Reliability Engineer (SRE) with over 16 years of experience.
She is an advocate at Honeycomb for the SRE and Observability communities, and previously was an SRE working on products ranging from the Google Cloud Load Balancer to Google Flights.

We look forward to welcoming Liz at linux.conf.au 2022.


## Main Conference Schedule Now Available!

We have published the [schedule](../../schedule/) for the Main Conference across Saturday 15 and Sunday 16 January 2022.
While we had a slow start with submissions, we are very happy with the proposals that we ended up with and the talks we have selected to make up the conference this year.
We would like to thank all of the people that submitted a talk proposal to the conference.


## Would you like to volunteer?

We still have some positions available for volunteers at linux.conf.au 2022.
If you are interested in helping out, please take a look at our [Volunteers page](../../attend/volunteer/) for all the details as well as instructions on how to apply.
