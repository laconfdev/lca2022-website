---
layout: post
title: "Keynote: Kathy Reid"
card: keynote-kathy.77c8d7c4.png
---

<p class="lead">Today we're announcing our third keynote speaker for linux.conf.au 2022, Kathy Reid!</p>

<p><img class="img-fluid float-sm-right profile-pic" src="../../media/img/keynote/kathy_reid.jpg" alt="Kathy Reid" width="250" /></p>

Kathy will be speaking about how communities are systems too, and how thinking about them in the same ways we think about computers can reveal insights into creating and maintaining them.

Many of us work with systems in our professional lives.
Most likely, they take the form of cyber- or cyber-physical systems, where the constituent components - boxen, VAXen, Docksen, ESPen, are quite literally connected - possibly over some form of internet protocol.
These systems may be orchestrated or organic - but always serve a purpose.
They can co-operate, collide and compete with each other (and then there were 15 standards).
They can operate loosely or be tightly controlled.
They exist for a brief period of time (less brief if they contain COBOL), until they are disassembled, disaggregated and decommed back to electrons and e-waste.

But what about the people in those systems? How are they connected? Why are they part of the system? Or not? What roles do they serve? What are their inputs and outputs? Plot twist! Communities are systems too! The concepts we use for thinking about computer and cyber systems can also be used to uncover insights about communities, and how we go about creating, curating and concluding them.


## About Kathy Reid

Kathy Reid works at the intersection of open source, emerging technologies and the technical communities who shape and are shaped by them.
Over the last 20 years, she has held several technical and community leadership positions, including as Digital Platforms and Operations Manager at Deakin University, Director of Developer Relations at Mycroft AI, and President of Linux Australia.
More recently she has worked in voice and conversational AI at Mozilla and NVIDIA, helping to create speech technology that works well for everyone.

Kathy holds Arts and Science undergraduate degrees from Deakin University, an MBA (Computing) from Charles Sturt University, a Master in Applied Cybernetics (MAppCyber) from Australian National University, as well as several ITIL certifications.
In 2019, she was one of 16 people from across the world chosen as the first to co-create a Masters Program in a new branch of engineering at the Australian National University's School of Cybernetics, where she is now a PhD Candidate, researching voice dataset documentation as a way to create more responsible technology.

We look forward to welcoming Kathy at linux.conf.au 2022.
