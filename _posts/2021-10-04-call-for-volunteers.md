---
layout: post
title: "Call for Volunteers"
card: call-volunteers.52e4ad24.png
---

<p class="lead">The linux.conf.au 2022 team would like to officially welcome applications for volunteers to help with the conference.</p>

linux.conf.au is a grass-roots, community-driven event, which wouldn't be successful without the wonderful people who are willing to lend us a hand.
This year, we're again seeking volunteers to fulfil a variety of roles to make this conference a success.

We have four roles available for volunteers in 2022: speaker wrangler, MC, AV, and room monitor.
Our speaker wranglers will coordinate speakers within each room, ensuring they make their way through the backstage during the event and deliver their presentation.
Each of our rooms will have a MC, who will introduce our speakers to the audience and help with timekeeping for sessions.
A critical component of the conference is our AV team, especially in an online format, who manage the audio and visual elements of every presentation and deliver the stream to our delegates.
Finally our room monitors will keep an eye on the chat during the event, helping to answer queries about the conference as well as facilitate question/answer sessions at the end of a presentation.

With the conference being online, you can be located anywhere in the world, as long as you are willing to be online during the conference hours.
It is important to note that the conference timezone will be Australian Eastern Daylight Time (AEDT - UTC+11).
Depending on your role, most volunteers will be required from 8am on the days of the conference.

In return for your assistance, we will provide free registration to the conference as well as a food allowance to provide you with breakfast, lunch, or dinner, whichever you choose.
Depending on the number of volunteers and work load, we'll also try our best to let everyone go and attend a few talks.

Our past volunteers will tell you that helping out with linux.conf.au is extremely rewarding, and we are expect to see many familiar faces again.
You'll get a chance to do things you might not normally get to do, an opportunity to meet some of the leaders in our industry, and the ability to form lifelong friendships.

If you'd like to be a volunteer for linux.conf.au 2022, it's easy to apply.
Visit our [Volunteers page](../../attend/volunteer/) for all the details as well as instructions on how to apply.
If you have any questions, send us an [email](mailto:volunteers@lca2022.linux.org.au).
