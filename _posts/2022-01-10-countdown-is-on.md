---
layout: post
title: The countdown is on
card: launch.1383c1ee.png
---

<p class="lead">There are less than four days until linux.conf.au 2022 begins!</p>

We are down to the last few days to buy your ticket for LCA2022, so we wanted to give you a quick update on what is happening this year.

## Conference Schedule

As usual the conference will start with Miniconfs on Friday 14 January, followed by two days of main conference on Saturday 15 and Sunday 16 January.
This year we have four Miniconfs: GO GLAM meets Community, Kernel, Open Hardware, and System Administration.
There will be over 30 sessions running at the Miniconfs, with talks specialised around the four topic areas.
We then have 48 sessions to choose from across the two days of main conference talks on a range of topics related to open technologies.
You can view the [schedule](../../schedule/) today to start choosing which talks to attend.

For those new to the conference, we welcome you to attend the Newcomers Session on Thursday evening from 5:30pm AEDT.
This is a great way to learn about the history of the conference and find out how to make the most of attending your first LCA.

## Keynotes

We are very excited to have four wonderful keynote presentations at linux.conf.au 2022.

* Brian Kernighan - [Friday morning](../../schedule/presentation/95/)
* Kathy Reid - [Friday evening](../../schedule/presentation/97/)
* Jono Bacon - [Saturday morning](../../schedule/presentation/96/)
* Liz Fong-Jones - [Sunday morning](../../schedule/presentation/98/)

## Special Guest for PDNS

Our Professional Delegates Networking Session (PDNS) is open to all professional-level ticket holders, including Sponsors, Contributors, Professionals, and our wonderful Speakers.
This year we will be hearing from our special guest speaker [Antony Green](../../schedule/presentation/99/) with his presentation Election Night Analysis: Art or Science.

## Tickets and Merchandise

Tickets are still on sale to attend the conference, with our standard Hobbyist ticket only AUD$70.
Please head to our [tickets page](../../attend/tickets/) to find details on all of the ticket types and their inclusions.
Ready to purchase your ticket? Head to your [dashboard](../../dashboard/) to secure yours today.

If you haven't already done so, remember to purchase your [shirts and other merchandise](../../attend/shirts/).
We have a range of items available this year, in two different designs.

## LCA Local

Given the current situation with COVID-19, some of our planned LCA Local events are unfortunately not able to proceed.
Everyone will still have access to the full conference via the online platform.

If you are in a region that is able to host events, and you would like to have a face-to-face meetup of LCA delegates in your local area, please visit the [events page](../../programme/events/) for details on how to register your LCA Local event for everyone to see.

## Get Excited!

We are looking forward to welcoming you all to linux.conf.au 2022 later this week.
If you have bought a ticket, keep an eye out for your registration email which will have details on how to access the conference.
