---
layout: post
title: "Keynote: Jono Bacon"
card: keynote-jono.7079e067.png
---

<p class="lead">We are pleased to announce that Jono Bacon is another of our keynote speakers for linux.conf.au 2022.</p>

<p><img class="img-fluid float-sm-right profile-pic" src="../../media/img/keynote/jono_bacon.jpg" alt="Jono Bacon" width="250" /></p>

Jono will be presenting The Conflict and Burnout Survival Guide: Handling When Things Go Wrong.

There is one problem every community will face at some point: conflict.
People are people, personalities are personalities, but how do you ensure early detection of potential conflict, engage with those involved effectively, and resolve any conflict quickly with a high degree of trust?
Jono will show you how you can do this with a pragmatic framework for every step of the conflict process, giving you a clear set of tools to deal with conflict as and when it may occur.
He will also go much deeper and dig into other challenging moments, whether there is conflict or not, such as mismatched expectations, uncertainty, burnout, uncertainty, company/community relations, and much more.


## About Jono Bacon

Jono Bacon is a leading community and collaboration speaker, author, and podcaster.
He is the founder of Jono Bacon Consulting which provides community strategy/execution, workflow, and other services.
He previously served as director of community at GitHub, Canonical, XPRIZE, and OpenAdvantage.
He is the author of 'People Powered: How communities can supercharge your business, brand, and teams' and The Art of Community, a columnist for Forbes and opensource.com, founder of the Community Leadership Summit, founder of Conversations With Bacon, and co-founder of Bad Voltage.

We look forward to welcoming Jono at linux.conf.au 2022.


## Miniconf Schedules Now Available!

We have published the [schedule](../../schedule/) for our Miniconfs on Friday 14 January 2022.
A reminder that our Miniconfs are:

* GO GLAM meets Community
* Kernel
* Open Hardware
* System Administration

We are excited to see all of the talks at the conference next year.
