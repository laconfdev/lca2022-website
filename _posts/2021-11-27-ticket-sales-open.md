---
layout: post
title: Ticket Sales now open!
card: ticket-sales.d0b65c68.png
---

<p class="lead">We are pleased to announce that ticket sales for linux.conf.au 2022 have now launched!</p>

This year the conference is taking place online, worldwide, from Friday 14 to Sunday 16 January.
As usual the conference will start with Miniconfs on Friday 14 January, followed by two days of main conference on Saturday 15 and Sunday 16 January.
For the Professional-level ticket holders, we will be having a special guest speaker present at our Professional Delegates Networking Session (PDNS) on the Saturday evening - keep an eye out for more details soon!

As with last year, the online format means we can offer tickets at a lower price.
This does mean that there are no Early Bird tickets.
Jump onto your [dashboard](../../dashboard/) to secure yours today.

## Ticket Prices

* Miniconf Only (Friday only pass): $25
* Student/Concession: $30
* Hobbyist: $70
* Professional: $125
* Contributor: $300

For full details and inclusions, take a look at the [tickets](../../attend/tickets/) page.
All prices are AUD, including GST.

## Open Hardware Miniconf Kits

The Open Hardware Miniconf is again producing hardware kits that will be available for purchase through the conference website.
Stocks are strictly limited, so we encourage you to get in early if you would like to secure a kit this year.
You can find out more about the hardware in their [announcement](http://www.openhardwareconf.org/wiki/OHMC2022_Announcement).

## Financial Assistance

With the conference running online, we hope it is easier for more people to attend given the lower ticket price and no travel or accommodation costs.
However we understand that the past couple of years have been a challenge for many people financially and some may still struggle to find the means to join us.

Thanks to the support of our Outreach and Inclusion sponsor, Arm, we have funds set aside to provide financial assistance to linux.conf.au attendees who might need it. This program is part of our outreach and inclusion efforts for linux.conf.au.

To find out more about our financial assistance and how to apply, read our [assistance page](../../attend/assistance/).

## Pick your own LCA2022 branded merchandise

If you've attended linux.conf.au in the past you know we usually hand out branded t-shirts.
Once again we are offering a wide range of branded merchandise through RedBubble.
You can buy t-shirts, laptop cases, socks, tapestries and plenty more.

To make things more exciting, we have two different designs available this year.
You can choose from the [LCA2022 Sanitiser Tux](https://www.redbubble.com/shop/ap/94005855) or the [linux.conf.au 2022 Community](https://www.redbubble.com/shop/ap/94005406) designs.
We look forward to seeing you with your merchandise!
